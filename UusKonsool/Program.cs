﻿using System;

namespace UusKonsool
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Inimene henn = new Inimene { EesNimi = "Henn", PereNimi = "Sarv", Vanus = 64 };
            Inimene ants = new Inimene("Ants", "Saunamees");
            Inimene peeter = ("Peeter", "Suur"); peeter.Vanus = 10;
            Inimene toots = "Joosep Toots";
            Inimene luarvik = "Luarvik";

            string testimiseks = "Henn on ilus poiss";

            Console.WriteLine(testimiseks[4..^6]);

            TeeMidagi(4);
            TeeMidagi(4.0);
            TeeMidagi(peeter);
            TeeMidagi(henn);
            TeeMidagi(DateTime.Now);

            int valik = 7;

            string vastus = valik switch
            {
                1 => "Esmas",
                2 => "Teisi",
                3 => "Kolma",
                4 => "Nelja",
                _ => "miski muu "
            } + "päev";

            Console.WriteLine(vastus);

            string mida = "Nimi";
            vastus = mida switch
            {
                "Nimi" => henn.Nimi,
                "Eesnimi" => henn.EesNimi,
                "Perenimi" => henn.EesNimi,
                "Vanus" => henn.Vanus.ToString(),
                _ => henn.ToString()
            };

            string kes = "loll";
            Inimene kesInimene = kes switch
            {
                "Henn" => henn,
                "Ants" => ants,
                _ => (Inimene)(("tundmatu", ""))
            };
            Console.WriteLine(kesInimene);


        }

        public static void TeeMidagi(object o)
        {
            switch (o)
            {
                case int i: Console.WriteLine($"täisarv {i}"); break;
                case double d: Console.WriteLine($"komaga arv {d}"); break;
                case Inimene i when i.Vanus < 18: Console.WriteLine($"laps {i}"); break;
                case Inimene i: Console.WriteLine($"inimene {i}"); break;
                default: Console.WriteLine($"minu jaoks tundmatu asi {o}"); break;
            }
        }

    }

    class Inimene
    {
        public string EesNimi { get; set; }
        public string PereNimi { get; set; }

        public int Vanus { get; set; }


        public Inimene() { }

        public Inimene(string eesNimi, string perenimi)
            => (EesNimi, PereNimi) = (eesNimi, perenimi);

        public static implicit operator Inimene((string eesNimi, string pereNimi) x)
            => new Inimene(x.eesNimi, x.pereNimi);

        public static implicit operator Inimene(string nimi)
             => new Inimene(nimi.Split(' ')[0], (nimi + " ").Split(' ')[1]);

        public string Nimi => $"{EesNimi} {PereNimi}";

        public override string ToString() => Nimi;

    }

}
